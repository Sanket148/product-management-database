create database product_management;
use product_management;
CREATE TABLE Products (
 id int,
 Name varchar(50),
 Quantity varchar(50),
 price varchar(500)
);
CREATE TABLE Category (
 id int,
 Name varchar(50),
 description varchar(50)
);
CREATE TABLE Variations (
 id int,
 Name varchar(50),
 value varchar(50)
);
CREATE TABLE Product_Category_Map (
 product_id int,
 category_id int
);
CREATE TABLE Product_variation_Map (
 product_id int,
 variation_id int
);
show tables;

select * from Category;
select*from Products;

insert into Products (id, Name, Quantity, price) values (1 ,'Pen', '10','10');
insert into Products (id, Name, Quantity, price) values (2 ,'Book', '100','50');
insert into Products (id, Name, Quantity, price) values (3 ,'Notebook', '50','100');
insert into Products (id, Name, Quantity, price) values (4 ,'Eraser', '100','5');
insert into Products (id, Name, Quantity, price) values (5 ,'Mobile', '60','10000');
insert into Products (id, Name, Quantity, price) values (6 ,'TV', '10','20000');
insert into Products (id, Name, Quantity, price) values (7 ,'Laptop', '10','30000');
insert into Products (id, Name, Quantity, price) values (8 ,'Hardisk', '5','6000');
insert into Products (id, Name, Quantity, price) values (9 ,'Pen Drive', '5','500');
insert into Products (id, Name, Quantity, price) values (10 ,'Fridge', '8','15000');
insert into Product_Category_Map (product_id, category_id) values (1 ,1);
insert into Product_Category_Map (product_id, category_id) values (2 ,1);
insert into Product_Category_Map (product_id, category_id) values (3 ,1);
insert into Product_Category_Map (product_id, category_id) values (4 ,1);
insert into Product_Category_Map (product_id, category_id) values (5 ,2);
insert into Product_Category_Map (product_id, category_id) values (6 ,2);
insert into Product_Category_Map (product_id, category_id) values (7 ,2);
insert into Product_Category_Map (product_id, category_id) values (5 ,4);
insert into Product_Category_Map (product_id, category_id) values (6 ,4);
insert into Product_Category_Map (product_id, category_id) values (7 ,4);
insert into Product_Category_Map (product_id, category_id) values (10 ,2);
insert into Product_Category_Map (product_id, category_id) values (10 ,4);
insert into Product_Category_Map (product_id, category_id) values (7 ,3);
insert into Product_Category_Map (product_id, category_id) values (8 ,3);
insert into Product_Category_Map (product_id, category_id) values (9 ,3);
insert into Product_variation_Map (product_id, variation_id) values (1 ,1);
insert into Product_variation_Map (product_id, variation_id) values (1 ,2);
insert into Product_variation_Map (product_id, variation_id) values (5 ,1);
insert into Category (id, Name, description) values (1 ,'Stationary', 'All stationary products');
insert into Category (id, Name, description) values (2 ,'Home Applience', 'All home needs');
insert into Category (id, Name, description) values (3 ,'Computer', 'IT stuff');
insert into Category (id, Name, description) values (4 ,'Electronics', 'Electronics devices');
insert into Variations (id, Name, value) values (1 ,'Color', 'Blue,Black,Red');
